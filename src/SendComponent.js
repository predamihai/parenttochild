import React, {Component} from 'react';
class SendComponent extends Component{
  constructor(props){
      super(props);
      this.state={
          item : ''
      }
      this.handleChange=(evt)=>{
          this.setState({
              [evt.target.name]:evt.target.value
          })
          //console.log([evt.target.name])
      }
      this.handleClick=()=>{
          this.props.action(this.state.item);
      }
  }
  
  
  render(){
    return (
    <div>
        <input type="text" name="item" onChange={this.handleChange} />
        <br/>
        <input type="button" value="click me" onClick={this.handleClick} />
    </div>
    );
  }
}
export default SendComponent;