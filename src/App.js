import React, {Component} from 'react';
import ReceivedComponent from './ReceivedComponent';
import SendComponent from './SendComponent';
import './App.css';


class App extends Component{
  constructor(){
    super();
    this.state={
      item:'nothing'
    }
    this.changeItem=(updatedItem)=>{
      this.setState({
        item:updatedItem
      })
    }
  }
  
  render(){
    console.log("App component state:",this.state);
    return (
    <div className="App">
     
    
     send Component
     <SendComponent action={this.changeItem}/>
      <br/>
      
      Received Component
     <ReceivedComponent item={this.state.item}/>
    </div>
    );
  }
}


export default App;
